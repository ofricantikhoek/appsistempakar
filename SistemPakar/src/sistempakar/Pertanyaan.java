/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistempakar;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LRSDA
 */
public class Pertanyaan {
    private int id;
    private int superid;
    private String sectionstring;
    private String pertanyaan;
    private List<String> jawaban = new ArrayList<>();
    private Pertanyaan subpertanyaan = null;
    private boolean value = false;
    private int key;
    private List<Integer> section = new ArrayList<>();
    
    Pertanyaan()
    {
        this.section = new ArrayList<>();
        
    }

    /**
     * @return the pertanyaan
     */
    public String getPertanyaan() {
        return pertanyaan;
    }

    /**
     * @param pertanyaan the pertanyaan to set
     */
    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }
    
    public int getJawabanSize()
    {
        return this.jawaban.size();
    }
    
    public void addJawaban(String jawab)
    {
        this.jawaban.add(jawab);
    }
    public String getJawaban(int index)
    {
        return this.jawaban.get(index);
    }


    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the superid
     */
    public int getSuperid() {
        return superid;
    }

    /**
     * @param superid the superid to set
     */
    public void setSuperid(int superid) {
        this.superid = superid;
    }

    /**
     * @return the section
     */
    public String getSectionsString() {
        return sectionstring;
    }

    /**
     * @param section the section to set
     */
    public void setSectionstring(String section) {
        this.sectionstring = section;
        extractSection();
    }

    /**
     * @return the subpertanyaan
     */
    public Pertanyaan getSubpertanyaan() {
        return subpertanyaan;
    }

    /**
     * @param subpertanyaan the subpertanyaan to set
     */
    public void setSubpertanyaan(Pertanyaan subpertanyaan) {
        this.subpertanyaan = subpertanyaan;
    }
    
    public boolean cekvalue()
    {
        if(getSubpertanyaan() != null)
        {
            return this.isValue() && getSubpertanyaan().cekvalue();
        }
        else
        {
            return this.isValue();
        }
    }
    
    public void cekJawaban(int jawab)
    {
        if(jawab == this.getKey())
        {
            this.value = true;
        }
        
    }
    
    public void extractSection()
    {
        if(sectionstring != "")
        {
            char[] x = sectionstring.toCharArray();
            int tamp ;
            for(int i = 0; i<sectionstring.length()-1;i+=2)
            {
                tamp = Integer.parseInt(""+x[i]);
                System.out.println(tamp);
                getSection().add(tamp);
            }
            
        }
        
        
        
        
    }

    /**
     * @return the key
     */
    public int getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(int key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public boolean isValue() {
        return value;
    }

    /**
     * @return the section
     */
    public List<Integer> getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(List<Integer> section) {
        this.section = section;
    }
}
