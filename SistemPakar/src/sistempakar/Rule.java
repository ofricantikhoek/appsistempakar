/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistempakar;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LRSDA
 */
public class Rule {
    private String id;
    private String text;
    private List<String> jawaban = new ArrayList<>();
    private boolean value;
    
    Rule(){
        
    }
          
    
    Rule(String id, List<String> jawaban)
    {
        this.id = id;
        this.jawaban = jawaban;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the jawaban
     */
    public List<String> getJawaban() {
        return jawaban;
    }

    /**
     * @param jawaban the jawaban to set
     */
    public void setJawaban(List<String> jawaban) {
        this.jawaban = jawaban;
    }

    /**
     * @return the value
     */
    public boolean isValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(boolean value) {
        this.value = value;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
    
    public void addJawaban(String jawaban)
    {
        this.jawaban.add(jawaban);
    }
    
    
}
