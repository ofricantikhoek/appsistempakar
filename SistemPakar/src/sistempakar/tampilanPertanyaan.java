/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistempakar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ButtonGroup;

/**
 *
 * @author Cyndy Alisia
 */
public class tampilanPertanyaan extends javax.swing.JFrame {
    int next;
    List<Integer> stack = new ArrayList<>();
    List<Pertanyaan> list = new ArrayList<>();
    Pertanyaan current;
    int jawabanUser=0;
    
    
   
    public tampilanPertanyaan() {
        initComponents();
        next=0;
        groupButton();
        stack.add(0);
        //tampikanPertanyaan();
        ambilPertanyaan();
        current = list.get(next);
        tampikanPertanyaan();
    }
    

    public void ambilPertanyaan()
    {
        Pertanyaan tamp = new Pertanyaan();
        String sql ="Select id, pertanyaan, jawab1, jawab2, jawab3, super, key, section from daftarTanya";
        
        try (Connection connection = Connectiondb.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql)) {
            
            while(resultSet.next()) {
               
                tamp.setId(resultSet.getInt("id"));
                tamp.setSuperid(resultSet.getInt("super"));
                tamp.setPertanyaan(resultSet.getString("pertanyaan"));
                tamp.addJawaban(resultSet.getString("jawab1"));
                tamp.addJawaban(resultSet.getString("jawab2"));
                if (!"".equals(resultSet.getString("jawab3")))
                {
                    tamp.addJawaban(resultSet.getString("jawab3"));
                }
                String key = resultSet.getString("key");
                tamp.setKey(Integer.parseInt(key));
                tamp.setSectionstring(resultSet.getString("section"));
                
                System.out.println(tamp.getPertanyaan());
                if(tamp.getSuperid() == 0)
                {
                    list.add(tamp);
                }
                else
                {
                    for(Pertanyaan x : list)
                    {
                        Pertanyaan y = x;
                        while(y != null)
                        {
                            if(y.getId() == tamp.getSuperid() && y.getSubpertanyaan() == null)
                            {
                                y.setSubpertanyaan(tamp);
                            }
                            else if(y.getSubpertanyaan() != null)
                            {
                                y = y.getSubpertanyaan();
                            }
                            else
                            {
                                y = null;
                            }
                        }
                    }
                }

                
                tamp = new Pertanyaan();
                System.out.println("DATA found in database");
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        System.out.println(list.size());
        
        for(Pertanyaan x : list)
        {
            System.out.println(x.getPertanyaan());
        }

    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        rbJawaban1 = new javax.swing.JRadioButton();
        rbJawaban2 = new javax.swing.JRadioButton();
        rbJawaban3 = new javax.swing.JRadioButton();
        btnNext = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPertanyaan = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("PERTANYAAN !");

        rbJawaban1.setText("jRadioButton1");
        rbJawaban1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbJawaban1MouseClicked(evt);
            }
        });

        rbJawaban2.setText("jRadioButton2");
        rbJawaban2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbJawaban2MouseClicked(evt);
            }
        });

        rbJawaban3.setText("jRadioButton3");
        rbJawaban3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbJawaban3MouseClicked(evt);
            }
        });

        btnNext.setText("Next");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        txtPertanyaan.setColumns(5);
        txtPertanyaan.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        txtPertanyaan.setLineWrap(true);
        txtPertanyaan.setRows(5);
        txtPertanyaan.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtPertanyaan.setEnabled(false);
        jScrollPane1.setViewportView(txtPertanyaan);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(rbJawaban2)
                            .addComponent(rbJawaban1)
                            .addComponent(rbJawaban3)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(jLabel1)))
                .addContainerGap(23, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnNext)
                .addGap(41, 41, 41))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbJawaban1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbJawaban2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbJawaban3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                .addComponent(btnNext)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        // TODO add your handling code here:
        current.cekJawaban(jawabanUser);
        stack.add(next);
        if(current.isValue() && current.getSubpertanyaan() != null)
        {
            current = current.getSubpertanyaan();
        }
        else
        {
            ++next;
            if(next == list.size())
            {
                hasil end = new hasil(list);
                end.setVisible(true);
                this.dispose();
            }
            else
            {
                current = list.get(next);
            }
        }

        
        tampikanPertanyaan();
    }//GEN-LAST:event_btnNextActionPerformed

    private void rbJawaban1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbJawaban1MouseClicked
        // TODO add your handling code here:
        jawabanUser = 1;
    }//GEN-LAST:event_rbJawaban1MouseClicked

    private void rbJawaban2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbJawaban2MouseClicked
        // TODO add your handling code here:
        jawabanUser = 2;
    }//GEN-LAST:event_rbJawaban2MouseClicked

    private void rbJawaban3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbJawaban3MouseClicked
        // TODO add your handling code here:
        jawabanUser = 3;
    }//GEN-LAST:event_rbJawaban3MouseClicked

    public void tampikanPertanyaan(){
        txtPertanyaan.setText(current.getPertanyaan());
        rbJawaban1.setText(current.getJawaban(0));
        rbJawaban2.setText(current.getJawaban(1));
        if(current.getJawabanSize() >2)
        {
            rbJawaban3.setVisible(true);
            rbJawaban3.setText(current.getJawaban(2));
        }
        else
        {
            rbJawaban3.setVisible(false);
        }
        
        
        System.out.println(next);
    }

    private void groupButton()
    {
        ButtonGroup tipe = new ButtonGroup();
        
        tipe.add(rbJawaban1);
        tipe.add(rbJawaban2);
        tipe.add(rbJawaban3);

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNext;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rbJawaban1;
    private javax.swing.JRadioButton rbJawaban2;
    private javax.swing.JRadioButton rbJawaban3;
    private javax.swing.JTextArea txtPertanyaan;
    // End of variables declaration//GEN-END:variables
}
