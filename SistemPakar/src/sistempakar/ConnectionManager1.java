package sistempakar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author RienCus
 */
public class ConnectionManager1 {
    private static String url = null;
    
    public static Connection getConnection(){
        if(url==null) {
            //place dynamic location here later
            url = "jdbc:sqlite:../data.db";
        }
        try {
            return DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return null;
    }
}
