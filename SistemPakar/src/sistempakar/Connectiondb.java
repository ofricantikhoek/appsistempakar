/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistempakar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author LRSDA
 */
public class Connectiondb {
        private static String url = null;
    
    public static Connection getConnection(){
        if(url==null) {
            //place dynamic location here later
            url = "jdbc:sqlite:../data.db";
        }
        try {
            return DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return null;
    }
}
